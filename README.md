# Proyek Akhir PPW Kelompok 3

[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

**Nama-Nama Anggota Kelompok:**
1. Alin Fathul Ilmi - 1906399972
2. Andre Septiano - 1906398313 
3. Falih Mafazan - 1806205376
4. Ruly Achmad Gemilang Gultom - 1906399915

**Link Herokuapp:** https://proyek-akhir-ppw.herokuapp.com/

[pipeline-badge]: https://gitlab.com/andreseptiano169/proyek-akhir-kelompok-3/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/andreseptiano169/proyek-akhir-kelompok-3/badges/master/coverage.svg
[commits-gl]: https://gitlab.com/andreseptiano169/proyek-akhir-kelompok-3/-/commits/master
