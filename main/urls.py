from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.index, name='index'),
    path('add_new_post/', views.create, name='create'),
    path('detail/<int:detail_id>/', views.detail, name='detail'),
    path('delete/<int:delete_id>/', views.delete, name='delete'),
    path('search/', views.search, name='search'),
    path('rate/', views.rate, name='rate'),
]
