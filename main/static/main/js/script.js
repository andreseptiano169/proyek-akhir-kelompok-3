const one = document.getElementById('first')
const two = document.getElementById('second')
const three = document.getElementById('third')
const four = document.getElementById('fourth')
const five = document.getElementById('fifth')

const form = document.querySelector('.rate-form')
const confirmBox = document.getElementById('confirm-box')
const csrf = document.getElementsByName('csrfmiddlewaretoken')

const handleStarSelect = (size) => {
    const children = form.children
    for (let i = 0; i < children.length; i++) {
        if (size >= i) {
            children[i].classList.add('checked')
        } else {
            children[i].classList.remove('checked')
        }
    }
}

const handleSelect = (selection) => {
    switch(selection) {
        case 'first': {
            handleStarSelect(1)
            return
        }

        case 'second' : {
            handleStarSelect(2)
            return
        }

        case 'third' : {
            handleStarSelect(3)
            return
        }

        case 'fourth' : {
            handleStarSelect(4)
            return
        }

        case 'fifth' : {
            handleStarSelect(5)
            return
        }

        default: {
            handleStarSelect(0)
        }
    }
}

const getNumVal = (stringValue) => {
    let numVal;
    if (stringValue === 'first') {
        numVal = 1
    } else if (stringValue === 'second') {
        numVal = 2
    } else if (stringValue === 'third') {
        numVal = 3
    } else if (stringValue === 'fourth') {
        numVal = 4
    } else if (stringValue === 'fifth') {
        numVal = 5
    } else {
        numVal = 0
    }
    return numVal
}

if (one) {
    const arr = [one, two, three, four, five]

    arr.forEach(item=>item.addEventListener('mouseover', (event) => {
        handleSelect(event.target.id)
    }))

    arr.forEach(item => item.addEventListener('click', (event) => {
        const val = event.target.id
        form.addEventListener('submit', e => {
            e.preventDefault()
            const id = e.target.id
            const numVal = getNumVal(val)
            const innerhtml = '<h1>Berhasil memberi rating: ' + numVal + '</h1>'
    
            $.ajax({
                type:'POST',
                url:'/rate/',
                data: {
                    'csrfmiddlewaretoken':csrf[0].value,
                    'element_id':id,
                    'value':numVal,
                },
                success: function(response) {
                    console.log(response)
                    confirmBox.innerHTML = innerhtml
                },
                error: function(response) {
                    console.log(response)
                    confirmBox.innerHTML = '<h1>Gagal memberi rating</h1>'
                }
            })
        })
    }))
}