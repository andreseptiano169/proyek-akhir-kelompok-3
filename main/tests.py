from .models import Post, Rating
from django.test import TestCase, Client
from django.contrib.auth.models import User 
from django.urls import resolve
from .views import index, rate

# Create your tests here.
class MainTestCase(TestCase):
    def setUp(self):
        self.user_1 = User.objects.create_user('Chevy Chase', 'chevy@chase.com', 'chevyspassword')
        self.user_2 = User.objects.create_user('Jim Carrey', 'jim@carrey.com', 'jimspassword')
        p1 = Post.objects.create(title="p1", body="body", user=self.user_1)
        r1 = Rating.objects.create(post=p1, user=self.user_1, score=5)
        r2 = Rating.objects.create(post=p1, user=self.user_2, score=3)
    
    def test_peserta_di_database(self):
        p1 = Post.objects.get(title="p1")

        self.assertEqual(p1.title, 'p1')
        self.assertEqual(p1.body, 'body')
        self.assertEqual(p1.user, self.user_1)

    def rating_test(self):
        p1 = Post.objects.get(title="p1")
        r1 = Rating.objects.get(post=p1, user=self.user_1)
        r2 = Rating.objects.get(post=p1, user=self.user_2)

        self.assertEqual(r1.rate, 5)
        self.assertEqual(r2.rate, 3)    

    def tearDown(self):
        self.user_1.delete()
        self.user_2.delete()