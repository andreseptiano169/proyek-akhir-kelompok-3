from django.http.response import JsonResponse
from django.shortcuts import redirect, render
from django.http import HttpResponse
from django.contrib.auth.models import User 
from .models import Post, Rating
from .forms import PostForm

# Create your views here.

def index(request):
    all_news = Post.objects.all().order_by('-id')

    context = {
        'all_news':all_news,
    }

    return render(request, 'main/index.html', context)

def create(request):
    if request.method == 'POST':
        form = PostForm(request.POST)
        form.instance.user = request.user
        if form.is_valid():
            form.save()
            return redirect('/')
    else:
        form = PostForm()
    
    context = {
        'form': form
        }

    return render(request, 'main/create.html', context)

def detail(request, detail_id):
    post_detail = Post.objects.get(id=detail_id)
    author = str(post_detail.user.username)
    title = post_detail.title
    ratings = Rating.objects.filter(post=post_detail)
    ratingcount = 0
    ratingsum = 0
    for i in range (len(ratings)):
        ratingcount += 1
        ratingsum += ratings[i].score
    
    if ratingcount == 0:
        ratingret = "No Rate Yet"
    else:
        ratingret = "{average:.2f}".format(average=(ratingsum/ratingcount))

    isAuthor = False
    if request.user.is_authenticated:
        isAuthor = (author == str(request.user.username))

    context = {
        'post_detail':post_detail,
        'author':author,
        'isAuthor':isAuthor,
        'title':title,
        'rating':ratingret,
    }
    
    return render(request, 'main/detail.html', context)

def delete(request, delete_id):
    Post.objects.filter(id=delete_id).delete()
    return redirect('main:index')

def search(request):
    if request.method == 'POST':
        searched = request.POST['searched']
        post = Post.objects.filter(title__contains=searched).order_by('-title').reverse()
        return render(request, 'main/searchresult.html', {'all_news' : post})
    else:
        return redirect('main:index')

def rate(request):
    element_id = request.POST.get('element_id')
    value = request.POST.get('value')
    post = Post.objects.get(id=element_id)
    rating = Rating.objects.filter(post=post, user=request.user).first()
    
    if rating is not None:
        rating.score = value
        rating.save()
        return JsonResponse({'success':'true', 'score':value}, safe=False)

    else:
        rating = Rating.objects.create(post=post, user=request.user, score=value)
        rating.save()
        return JsonResponse({'success':'true', 'score':value}, safe=False)
